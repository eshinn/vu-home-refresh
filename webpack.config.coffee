module.exports =
  cache: true
  entry:
    common: [
      './src/js/libs/vwo.js'
    ]
  output:
    path: './dist/public/js'
    publicPath: '/js/'
    filename: '[name].js'
    checkFilename: '[chunkhash].js'
