module.exports = (grunt) ->
  grunt.config 'jade',

    options:
      basedir: 'src/views'

    dev:
      options:
        pretty: true

      files: [{
        cwd: 'src/views/pages'
        src: '**/*.jade'
        dest: 'dist/public'
        expand: true
        ext: '.html'
      }]

  grunt.loadNpmTasks 'grunt-contrib-jade'
