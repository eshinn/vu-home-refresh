autoprefixer = require 'autoprefixer'
autoReset = require 'postcss-autoreset'
inlineSVG = require 'postcss-inline-svg'
SVGo = require 'postcss-svgo'
fontMagician = require 'postcss-font-magician'
stylelint = require 'stylelint'

module.exports = (grunt) ->
  grunt.config 'postcss',
    options:
      processors: [
        fontMagician
          # foundries: 'bootstrap google'
        autoprefixer 
          browsers: [
            'last 2 versions'
            'ie 9'
            '> 2%'
          ]
        autoReset
          rulesMatcher: (rule)->
            rule.selector.match /^[a-zA-Z0-9]+/
          reset:
            margin: 0
            padding: 0
        inlineSVG
          path: './'
        SVGo
        stylelint
      ]
    dist:
      files: [{
        cwd: 'build/public/css'
        src: '**/*.css'
        dest: 'dist/public/css/'
        expand: true
        ext: '.css'
      }]

  grunt.loadNpmTasks 'grunt-postcss'