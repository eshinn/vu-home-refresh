module.exports = (grunt) ->
  grunt.config 'imagemin',

    jpg:
      options:
        progressive: true

      files: [{
        expand: true
        cwd: 'src/images'
        src: ['**/*.jpg','!ico/**']
        dest: 'dist/public/images/'
      }]

  grunt.loadNpmTasks 'grunt-contrib-imagemin'