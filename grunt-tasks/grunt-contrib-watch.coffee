module.exports = (grunt) ->
  grunt.config 'watch',

    options:
      spawn: false
      livereload: true
      
    jade:
      files: 'src/**/*.jade'
      tasks: [
        'clean:html'
        'html:dev'
      ]

    css:
      files: 'src/styl/**/*.{css,styl}'
      tasks: [
        'clean:css'
        'css:dev'
      ]

    # js:
    #   files: 'src/js/**/*.js'
    #   tasks: [
    #     'clean:js'
    #     'js:dev'
    #   ]

  grunt.loadNpmTasks 'grunt-contrib-watch'
