module.exports = (grunt) ->
  grunt.config 'clean',

    all:
      src: './{build,dist}'

    html:
      src: './{build,dist}/public/**/*.html'

    css:
      src: './{build,dist}/public/css/**/*.css'

    # js:
    #   src: ['./{build,dist}/public/**/*.js']

  grunt.loadNpmTasks 'grunt-contrib-clean'
