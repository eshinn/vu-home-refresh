module.exports = (grunt) ->
  grunt.config 'stylus',

    options:
      use: [require 'swiss-styl']

    dev:
      options:
        compress: false
      files: 'build/public/css/main.css': 'src/styl/index.styl'

    prod:
      options:
        compress: true
      files: 'build/public/css/main.css': 'src/styl/index.styl'

  grunt.loadNpmTasks 'grunt-contrib-stylus'
