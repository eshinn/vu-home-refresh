module.exports = (grunt) ->
  grunt.config 'favicons',

    options:
      html: 'build/ico.jade'
      HTMLPrefix: '/images/touch-icons/'
      indent: ''
      regular: true
      apple: true
      trueColor: false
      sharp: 0.00
      precomposed: true
      appleTouchBackgroundColor: '#036'
      appleTouchPadding: 15
      windowsTile: false
      coast: false
      tileBlackWhite: false
      tileColor: 'auto'
      firefox: false
      androidHomescreen: false

    dev:
      options:
        indent: ''
      src: 'src/images/ico/1024x1024_16-VU-000050_V_logo.png'
      dest: 'dist/public/images/touch-icons'

  grunt.loadNpmTasks 'grunt-favicons'
